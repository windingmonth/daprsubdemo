﻿using daprsubdemo.Model.Responses;
using ServiceStack;

namespace daprsubdemo.Model.Requests
{
    [Route("/hello")]
    [Route("/hello/{Name}")]
    public class HelloRequest : IReturn<HelloResponse>
    {
        public string Name { get; set; }
    }
}
