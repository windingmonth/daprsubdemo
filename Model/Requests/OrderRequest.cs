﻿using Dapr;
using daprsubdemo.Interface;
using ServiceStack;

namespace daprsubdemo.Model.Requests
{
    [Topic("pubsub", "daprsub/order")]
    [Route("/daprsub/order")]
    public class OrderRequest : DaprSub
    {
    }

    [Topic("pubsub", "daprsub/mediatorconsumer")]
    [Route("/daprsub/mediatorconsumer")]
    public class MediatorConsumerRequest : DaprSub
    {
    }
}