﻿namespace daprsubdemo.Model
{
    public class SocketMessage
    {
        public string Name { get; set; }
        public int OrderId { get; set; }
    }
}
