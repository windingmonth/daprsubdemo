﻿using daprsubdemo.Model.Requests;
using daprsubdemo.Model.Responses;
using ServiceStack;
using ServiceStack.Logging;

namespace daprsubdemo.Services
{
    public class HelloService : Service
    {
        public static ILog _logger = LogManager.GetLogger(typeof(HelloService));
        public object Any(HelloRequest request)
        {
            _logger.Info("hello");
            return new HelloResponse { Result = $"Hello, {request.Name}!" };
        }
    }
}
