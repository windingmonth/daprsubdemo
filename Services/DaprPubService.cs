﻿using Dapr.Client;
using daprsubdemo.Model;
using daprsubdemo.Model.Requests;
using daprsubdemo.Notifications;
using MediatR;
using ServiceStack;

namespace daprsubdemo.Services
{
    public class DaprPubService : Service
    {
        private DaprClient _daprClient;
        private IPublisher _mediator;
        public DaprPubService(DaprClient daprClient, IPublisher mediator)
        {
            _daprClient = daprClient;
            _mediator = mediator;
        }

        public object Any(DaprPubRequest request)
        {
            _daprClient.PublishEventAsync("pubsub", "daprsub/order", new SocketMessage { OrderId = new Random().Next(1, 10000) });
            return "OK";
        }

        public object Any(PublishMessageRequest request)
        {
            var message = new SocketMessage { Name = $"message_name_{new Random().Next(1, 1000)}" };
            _mediator.Publish(new MessageReceivedNotification(message), new CancellationToken());
            return "OK";
        }
    }
}
