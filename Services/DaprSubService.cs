﻿using daprsubdemo.Model.Requests;
using daprsubdemo.Model.Responses;
using ServiceStack;
using ServiceStack.Logging;

namespace daprsubdemo.Services
{
    public class DaprSubService : Service
    {
        public static ILog _logger = LogManager.GetLogger(typeof(DaprSubService));

        public object Post(OrderRequest request)
        {
            _logger.Info("DaprSub /daprsub/order===================");
            return new { success = true };
        }
        
        public object Post(MediatorConsumerRequest request)
        {
            _logger.Info("DaprSub /daprsub/mediatorconsumer===================");
            return new { success = true };
        }
    }
}
