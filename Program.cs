using daprsubdemo;
using daprsubdemo.Extensions;
using daprsubdemo.Handler;
using MediatR;
using ServiceStack;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers().AddDapr();
builder.Services.AddMediatR(typeof(DemoMessageHandler));

var app = builder.Build();
app.UseServiceStack(new AppHost());
app.UseRouting();
app.UseCloudEvents();
app.UseEndpoints(endpoints =>
{
    endpoints.MapDaprSubscribeHandler();   //�Զ��嶩���߼�
});
app.Run();