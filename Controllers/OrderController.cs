﻿//using Dapr;
//using daprsubdemo.Model;
//using Microsoft.AspNetCore.Mvc;
//using System.Text.Json.Serialization;

//namespace webapi.Controllers
//{
//    [ApiController]
//    [Route("")]
//    public class OrdersController : ControllerBase
//    {
//        [Topic("localpubsub", "daprsubdemoTopic")]
//        [Route("orders")]
//        [HttpPost()]
//        public async Task<ActionResult> Orders(Order order)
//        {
//            Console.WriteLine("Subscriber received : " + order);
//            return Ok();
//        }

//        [Topic("localpubsub", "daprsubmediatordemoTopic")]
//        [Route("MediatorConsumer")]
//        [HttpPost()]
//        public async Task<ActionResult> MediatorConsumer(SocketMessage message)
//        {
//            Console.WriteLine($"Subscriber received Mediator Message: {message.Name}");
//            return Ok();
//        }

//        [Route("hello")]
//        [HttpGet()]
//        public string Hello()
//        {
//            return "OK";
//        }
//    }


//    public record Order([property: JsonPropertyName("orderId")] int OrderId);
//}
