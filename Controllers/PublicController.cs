﻿//using Dapr;
//using Dapr.Client;
//using daprsubdemo.Model;
//using daprsubdemo.Notifications;
//using MediatR;
//using Microsoft.AspNetCore.Mvc;
//using webapi.Controllers;

//namespace daprsubdemo.Controllers
//{
//    [ApiController]
//    [Route("")]
//    public class PublicController : ControllerBase
//    {
//        private readonly IPublisher _mediator;

//        public PublicController(IPublisher mediator)
//        {
//            _mediator = mediator;
//        }

//        [Route("publicRandom")]
//        [HttpGet()]
//        public string PublicRandom([FromServices] DaprClient daprClient)
//        {
//            daprClient.PublishEventAsync("pubsub", "daprsubdemoTopic", new Order(new Random().Next(1, 10000)));
//            return "OK";
//        }

//        [Route("publicMessage")]
//        [HttpGet()]
//        public string publicMessage()
//        {
//            var message = new SocketMessage { Name = $"message_name_{new Random().Next(1, 1000)}" };
//            _mediator.Publish(new MessageReceivedNotification(message), new CancellationToken());
//            return "OK";
//        }
//    }
//}
