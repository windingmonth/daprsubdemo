﻿using daprsubdemo.Services;
using Funq;
using ServiceStack;
using ServiceStack.Logging;
using ServiceStack.Logging.NLogger;

namespace daprsubdemo
{
    public class AppHost : AppHostBase, IHostingStartup
    {
        public static ILog _logger = LogManager.GetLogger(typeof(AppHost));

        public void Configure(IWebHostBuilder builder) => builder
        .ConfigureServices(services =>
        {
            // Configure ASP.NET Core IOC Dependencies
        });

        public AppHost() : base("daprsubdemo", typeof(HelloService).Assembly)
        {
            LogManager.LogFactory = new NLogFactory();   //设置nlog
        }

        public override void Configure(Container container)
        {
            // Configure ServiceStack only IOC, Config & Plugins

            SetConfig(new HostConfig
            {
                DefaultRedirectPath = "/metadata",
                DebugMode = AppSettings.Get(nameof(HostConfig.DebugMode), false)
            });

            Plugins.Add(new CorsFeature());

            //全局过滤器
            this.GlobalRequestFilters.Add((req, res, requestDto) =>
            {
                _logger.Info($"\n req: {req}; \n res: {res}; \n requestDto: {requestDto}; \n");
            });
        }
    }
}