﻿using Dapr.Client;
using daprsubdemo.Notifications;
using MediatR;

namespace daprsubdemo.Handler
{
    public class DemoMessageHandler : INotificationHandler<MessageReceivedNotification>
    {
        private readonly DaprClient _daprClient;

        public DemoMessageHandler(DaprClient daprClient) 
        {
            _daprClient = daprClient;
        }

        public Task Handle(MessageReceivedNotification notification, CancellationToken cancellationToken)
        {
            Console.WriteLine($"MediatR works! (Received a message by {notification.Message.Name})");

            //发布消息到dapr
            _daprClient.PublishEventAsync("pubsub", "daprsub/mediatorconsumer", notification.Message);

            return Task.CompletedTask;
        }
    }
}
