﻿using Dapr;
using daprsubdemo.Interface;
using MediatR;
using ServiceStack;
using System.Text.Json;
using System.Text.RegularExpressions;

namespace daprsubdemo.Extensions
{
    public static class EndpointRouteBuilderExtensions
    {
        public static IEndpointConventionBuilder MapDaprSubscribeHandler(this IEndpointRouteBuilder endpoints)
        {
            if (endpoints is null)
            {
                throw new System.ArgumentNullException(nameof(endpoints));
            }

            
            return endpoints.MapGet("dapr/subscribe", async context =>
            {
                var dataSources = AppHostBase.Instance.RestPaths;

                var dataSourceLength = dataSources.Count;

                context.Response.ContentType = "application/json";

                using var writer = new Utf8JsonWriter(context.Response.BodyWriter);

                writer.WriteStartArray();

                for (var i = 0; i < dataSourceLength; i++)
                {
                    try
                    {
                        var dataSource = dataSources[i];

                        var dataSourceType = dataSources[i].RequestType;

                        var interfaces = dataSourceType.GetInterfaces();

                        if (!interfaces.ToList().Exists(i => i.Name == typeof(DaprSub).Name))
                        {
                            continue;
                        }

                        writer.WriteStartObject();

                        var attributeData = dataSourceType.GetCustomAttributesData();

                        var route = dataSource.Path;

                        writer.WriteString("route", route);

                        var topicAttr = attributeData.Where(a => a.AttributeType.Name == typeof(TopicAttribute).Name).FirstNonDefault();

                        var pubsubName = topicAttr != null ? topicAttr.ConstructorArguments[0].Value.ToString() : "";

                        var topic = topicAttr != null ? topicAttr.ConstructorArguments[1].Value.ToString() : "";

                        Regex rgx = new Regex(@"(?i)(?<=\{)(.*)(?=\})");
                        string settingKey = rgx.Match(topic).Value;
                        if (!settingKey.IsNullOrEmpty())
                        {
                            topic = topic.ReplaceAll("{" + settingKey + "}", HostContext.AppSettings.Get(settingKey, ""));
                        }

                        writer.WriteString("pubsubName", pubsubName);
                        writer.WriteString("topic", topic);
                        writer.WriteEndObject();
                    }
                    catch
                    {
                        throw;
                    }
                }

                writer.WriteEndArray();

                await writer.FlushAsync();
            });
        }
    }
}
